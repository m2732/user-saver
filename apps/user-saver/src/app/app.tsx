import React, { useState, useEffect } from 'react';
import usersData from './mocks/usersData.json';
import { GitHubUser } from '@code/types';

import Container from '@material-ui/core/Container';
import SearchBar from './components/SearchBar';
import UsersTable from './components/UsersTable';

const App = () => {
  const [users, setUsers] = useState<GitHubUser[]>([]);
  const [searchQuery, setSearchQuery] = useState('');

  const onSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(e.target.value);
  };

  useEffect(() => {
    setUsers(usersData.users);
  }, []);

  return (
    <Container>
      <SearchBar value={searchQuery} onChange={onSearchChange} />
      <UsersTable users={users} />
    </Container>
  );
};

export default App;
