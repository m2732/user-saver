import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { within } from '@testing-library/dom';

import App from './app';

describe('Search Bar', () => {
  it('should exist on page', () => {
    render(<App />);

    const searchBar = screen.queryByRole('searchbox');
    expect(searchBar).toBeTruthy();
  });

  it('should accept user inputted text', () => {
    render(<App />);

    const searchBar = screen.getByRole('searchbox');
    const searchText = 'mpottebaum';
    userEvent.type(searchBar, searchText);
    expect(searchBar).toHaveValue(searchText);
  });
});

describe('Users List', () => {
  it('should include a username column head', () => {
    render(<App />);

    const usernameHead = screen.queryByText('Username');
    expect(usernameHead).toBeTruthy();
  });

  it('should include a name column head', () => {
    render(<App />);

    const nameHead = screen.queryByText('Name');
    expect(nameHead).toBeTruthy();
  });

  it('should include a name column head', () => {
    render(<App />);

    const nameHead = screen.queryByText('Name');
    expect(nameHead).toBeTruthy();
  });

  it('should include a public repos column head', () => {
    render(<App />);

    const publicReposHead = screen.queryByText('Public Repos');
    expect(publicReposHead).toBeTruthy();
  });

  it('should include a public gists column head', () => {
    render(<App />);

    const publicGistsHead = screen.queryByText('Public Gists');
    expect(publicGistsHead).toBeTruthy();
  });

  it('should include a followers column head', () => {
    render(<App />);

    const followersHead = screen.queryByText('Followers');
    expect(followersHead).toBeTruthy();
  });

  it('should include a following column head', () => {
    render(<App />);

    const followingHead = screen.queryByText('Followers');
    expect(followingHead).toBeTruthy();
  });

  it('should include a created date column head', () => {
    render(<App />);

    const createdDateHead = screen.queryByText('Created Date');
    expect(createdDateHead).toBeTruthy();
  });

  it('should render users after component mounts', async () => {
    render(<App />);

    const usersListRowGroups = await screen.findAllByRole('rowgroup');
    const usersListTableBody = usersListRowGroups[1];
    const userRows = within(usersListTableBody).getAllByRole('row');
    expect(userRows).not.toHaveLength(0);
  });
});
