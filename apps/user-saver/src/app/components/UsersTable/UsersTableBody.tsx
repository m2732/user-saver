import { GitHubUser } from '@code/types';

import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

interface Props {
  users: GitHubUser[];
}

const UsersTableBody = ({ users }: Props) => {
  const formatDate = (dateStr: string) => {
    const date = new Date(dateStr);
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
  };
  const renderUserRows = () =>
    users.map((user) => {
      const {
        login: username,
        name,
        public_repos: publicRepos,
        public_gists: publicGists,
        followers,
        following,
        created_at: createdAt,
      } = user;
      const formattedDate = formatDate(createdAt);
      return (
        <TableRow key={username}>
          <TableCell>{username}</TableCell>
          <TableCell>{name}</TableCell>
          <TableCell>{publicRepos}</TableCell>
          <TableCell>{publicGists}</TableCell>
          <TableCell>{followers}</TableCell>
          <TableCell>{following}</TableCell>
          <TableCell>{formattedDate}</TableCell>
        </TableRow>
      );
    });
  return <TableBody>{renderUserRows()}</TableBody>;
};

export default UsersTableBody;
