import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const UsersTableHead = () => (
  <TableHead>
    <TableRow>
      <TableCell>Username</TableCell>
      <TableCell>Name</TableCell>
      <TableCell>Public Repos</TableCell>
      <TableCell>Public Gists</TableCell>
      <TableCell>Followers</TableCell>
      <TableCell>Following</TableCell>
      <TableCell>Created Date</TableCell>
    </TableRow>
  </TableHead>
);

export default UsersTableHead;
