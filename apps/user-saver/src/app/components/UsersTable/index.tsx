import { GitHubUser } from '@code/types';

import Table from '@material-ui/core/Table';
import UsersTableHead from './UsersTableHead';
import UsersTableBody from './UsersTableBody';

interface Props {
  users: GitHubUser[];
}

const UsersTable = ({ users }: Props) => (
  <Table>
    <UsersTableHead />
    <UsersTableBody users={users} />
  </Table>
);

export default UsersTable;
