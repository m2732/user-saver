import React from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';

interface Props {
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchBar = ({ value, onChange }: Props) => (
  <Container maxWidth="xs">
    <form>
      <TextField
        label="Search GitHub Users"
        name="Search Bar"
        type="search"
        value={value}
        onChange={onChange}
        fullWidth
      />
    </form>
  </Container>
);

export default SearchBar;
